﻿using IBM.Cloud.SDK.Core.Http;
using System.Reflection.PortableExecutable;
using System.Reflection;

namespace ndsApp1.Repositorios
{
    public class Repositorio : IRepositorio
    {
        private readonly HttpClient httpClient;

        public Repositorio(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        private JsonSerializerOptions OpcionesPorDefectoJSON =>
            new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

        public async Task<HttpResponseWrapper<T>> Get<T>(string url, string token = "", object header = null)
        {
            try
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                if (header != null)
                {
                    foreach (PropertyInfo propertyInfo in header.GetType().GetProperties())
                    {
                        var headerValue = propertyInfo.GetValue(header);

                        if (headerValue != null)
                        {
                            string headerKey = propertyInfo.Name.Replace("_", "-");
                            httpClient.DefaultRequestHeaders.Add(headerKey, ((string[])headerValue)[0] /*headerValue.ToString()*/);
                        }
                    }
                }

                var responseHTTP = await httpClient.GetAsync(url);

                if (responseHTTP.IsSuccessStatusCode)
                {
                    var response = await DeserializarRespuesta<T>(responseHTTP, OpcionesPorDefectoJSON);
                    return new HttpResponseWrapper<T>(response, false, responseHTTP);
                }
                else
                {
                    return new HttpResponseWrapper<T>(default, true, responseHTTP);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public async Task<HttpResponseWrapper<object>> Post<T>(string url, T enviar, string token = "")
        {
            var enviarJSON = System.Text.Json.JsonSerializer.Serialize(enviar);
            var enviarContent = new StringContent(enviarJSON, Encoding.UTF8, "application/json");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var responseHttp = await httpClient.PostAsync(url, enviarContent);
            return new HttpResponseWrapper<object>(null, !responseHttp.IsSuccessStatusCode, responseHttp);
        }

        public async Task<HttpResponseWrapper<object>> Put<T>(string url, T enviar)
        {
            var enviarJSON = System.Text.Json.JsonSerializer.Serialize(enviar);
            var enviarContent = new StringContent(enviarJSON, Encoding.UTF8, "application/json");
            var responseHttp = await httpClient.PutAsync(url, enviarContent);
            return new HttpResponseWrapper<object>(null, !responseHttp.IsSuccessStatusCode, responseHttp);
        }

        public async Task<HttpResponseWrapper<TResponse>> Post<T, TResponse>(string url, T enviar, string token = "", object header = null)
        {
            var enviarJSON = System.Text.Json.JsonSerializer.Serialize(enviar);
            var enviarContent = new StringContent(enviarJSON, Encoding.UTF8, "application/json");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (header != null)
            {
                foreach (PropertyInfo propertyInfo in header.GetType().GetProperties())
                {
                    var headerValue = propertyInfo.GetValue(header);

                    if (headerValue != null)
                    {                        
                        string headerKey = propertyInfo.Name.Replace("_", "-");
                        httpClient.DefaultRequestHeaders.Add(headerKey, ((string[])headerValue)[0] /*headerValue.ToString()*/);
                    }
                }
            }

            var responseHttp = await httpClient.PostAsync(url, enviarContent);

            if (responseHttp.IsSuccessStatusCode)
            {
                var response = await DeserializarRespuesta<TResponse>(responseHttp, OpcionesPorDefectoJSON);
                return new HttpResponseWrapper<TResponse>(response, false, responseHttp);
            }
            else
            {
                return new HttpResponseWrapper<TResponse>(default, true, responseHttp);
            }
        }

        public async Task<HttpResponseWrapper<object>> Delete(string url)
        {
            var responseHTTP = await httpClient.DeleteAsync(url);
            return new HttpResponseWrapper<object>(null, !responseHTTP.IsSuccessStatusCode, responseHTTP);
        }

        private async Task<T> DeserializarRespuesta<T>(HttpResponseMessage httpResponse, JsonSerializerOptions jsonSerializerOptions)
        {
            string respuesta = "";
            respuesta = await httpResponse.Content.ReadAsStringAsync();
            var we = System.Text.Json.JsonSerializer.Deserialize<T>(respuesta, jsonSerializerOptions);
            return System.Text.Json.JsonSerializer.Deserialize<T>(respuesta, jsonSerializerOptions);
        }

        private string headerType(string headerKey) 
        {
            if (headerKey == "UserAgent")
                headerKey = "User-Agent";
            else if (headerKey == "AcceptEncoding")
                headerKey = "Accep-tEncoding";
            else if (headerKey == "AcceptLanguage")
                headerKey = "Accept-Language";
            //else if (headerKey == "ContentType")
            //    headerKey = "Content-Type";
            //else if (headerKey == "ContentLength")
            //    headerKey = "Content-Length";
            else if (headerKey == "secchua")
                headerKey = "sec-ch-ua";
            else if (headerKey == "secchuamobile")
                headerKey = "sec-ch-ua-mobile";
            else if (headerKey == "XACCOUNTID")
                headerKey = "X-ACCOUNT-ID";
            else if (headerKey == "XAPPTOKEN")
                headerKey = "X-APP-TOKEN";
            else if (headerKey == "secchuaplatform")
                headerKey = "sec-ch-ua-platform";
            else if (headerKey == "SecFetchSite")
                headerKey = "Sec-Fetch-Site";
            else if (headerKey == "SecFetchMode")
                headerKey = "Sec-Fetch-Mode";
            else if (headerKey == "SecFetchDest")
                headerKey = "Sec-Fetch-Dest";

            return headerKey;
        }
    }
}
