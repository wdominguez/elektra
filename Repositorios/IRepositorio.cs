﻿namespace ndsApp1.Repositorios
{
    public interface IRepositorio
    {
        Task<HttpResponseWrapper<object>> Delete(string url);
        Task<HttpResponseWrapper<T>> Get<T>(string url, string token = "", object header = null);
        Task<HttpResponseWrapper<object>> Post<T>(string url, T enviar, string token = "");
        Task<HttpResponseWrapper<TResponse>> Post<T, TResponse>(string url, T enviar, string token = "", object header = null);
        Task<HttpResponseWrapper<object>> Put<T>(string url, T enviar);
    }
}
