using ndsApp1.Filters;
using ndsApp1.Repositorios;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IRepositorio, Repositorio>();
builder.Services.AddScoped<HttpClient>();
builder.Services.AddScoped<SessionFilter>();

var app = builder.Build();

//app.UseCors();
app.UseCors(builder =>
{
    builder.AllowAnyHeader();
    builder.WithMethods(new string[] { "GET", "POST", "OPTIONS" });
    builder.AllowAnyOrigin();
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();


app.Run();
