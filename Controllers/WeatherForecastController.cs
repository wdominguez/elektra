using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ndsApp1.Controllers
{
    [ApiController]
    [Route("api/Home")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        //public IEnumerable<WeatherForecast> Get()
        public async Task<ActionResult> Get()
        {
            var ww = await new prueba().prb();
            return Ok(ww);
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            //    TemperatureC = Random.Shared.Next(-20, 55),
            //    Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            //})
            //.ToArray();
        }

        [HttpPost("prb2")]
        public async Task<ActionResult> Post([FromBody] clase1 messageBody)
        {
            var w = await new prueba().prb2();
            return Ok(w);
        }

    }
}