﻿using ChatObject.Response;
using IBM.Cloud.SDK.Core.Http;
using Microsoft.Extensions.Configuration;
using ndsApp1.Filters;
using System.Reflection.PortableExecutable;
using System.Reflection;
using System;

namespace ndsApp1.Controllers
{
    [ApiController]
    [Route("Home")]
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRepositorio _repositorio;
        public HomeController(IConfiguration configuration, IRepositorio repositorio)
        {
            _configuration = configuration;
            _repositorio = repositorio;
        }

        [ServiceFilter(typeof(SessionFilter))]
        [HttpPost("prb")]
        public async Task<ActionResult> Post([Bind("message")][FromBody] AssistantMessageRequest messageBody)
        {
            string url = String.Format(_configuration.GetConnectionString("ndsOrq") + "message", "es");

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            string? sessionString = this.HttpContext.Items["sessionString"].ToString();
            Rootobject session = JsonConvert.DeserializeObject<Rootobject>(sessionString);
            var rr = JsonConvert.DeserializeObject<object>(sessionString);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////


            try
            {
                var res = await _repositorio.Post<AssistantMessageRequest, AssistantMessageResponse>(url, messageBody, "", session);
                //var r = res.Response;
                return Ok(res.Response);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            

            //string url = String.Format(_configuration.GetConnectionString("nds") + "Prueba/prb", "es");

            //AssistantMessageResponse ress = new AssistantMessageResponse();

            //if (messageBody.message.ToUpper() == "TICKET" || messageBody.message.ToUpper() == "COROUSEL")
            //{
            //    var res = await _repositorio.Post<AssistantMessageRequest, AssistantMessageResponse>(url, messageBody);
            //    ress = res.Response;                
            //}
            //else 
            //{
            //    var res = await _repositorio.Post<AssistantMessageRequest, Result>(url, messageBody);
            //    ress = res.Response.result;
            //}




            

        }

        [ServiceFilter(typeof(SessionFilter))]
        [HttpGet("newContext")]
        public async Task<ActionResult> newContext()
        {
            string sessionString = this.HttpContext.Items["sessionString"].ToString();
            SessionData session = JsonConvert.DeserializeObject<SessionData>(sessionString);
            var res = await _repositorio.Get<object>(sessionString,"",session);            

            return Ok(res.Response);
        }


        [HttpPost("getToken")]
        public async Task<ActionResult> getToken([Bind("accountId,client_name,client_segment,referrer,browser,os,device,language")][FromBody] SessionRequest tokenBody)
        {
            string urlAuth = String.Format(_configuration.GetConnectionString("ndsOrq") + "auth/getToken", "es");

            //SessionRequest ress = new SessionRequest();


            var authRes = await _repositorio.Post<SessionRequest, ChatSessionResponse>(urlAuth, tokenBody);

            var reeee = authRes.Response;


            return Ok(reeee);

        }
    }
}
