﻿namespace ndsApp1
{
    public class Rootobject
    {
        public string[]? Accept { get; set; }
        public string[]? Connection { get; set; }
        public string[]? Host { get; set; }

        [JsonProperty("User-Agent")]
        public string[]? User_Agent { get; set; }

        [JsonProperty("Accept-Encoding")]
        public string[]? Accept_Encoding { get; set; }

        [JsonProperty("Accept-Language")]
        public string[]? Accept_Language { get; set; }

        //[JsonProperty("Content-Type")]
        public string[]? Content_Type { get; set; }
        public string[]? Origin { get; set; }
        public string[]? Referer { get; set; }

        //[JsonProperty("Content-Length")]
        public string[]? Content_Length { get; set; }

        [JsonProperty("sec-ch-ua")]
        public string[]? sec_ch_ua { get; set; }

        [JsonProperty("sec-ch-ua-mobile")]
        public string[]? sec_ch_ua_mobile { get; set; }

        [JsonProperty("X-ACCOUNT-ID")]
        public string[]? X_ACCOUNT_ID { get; set; }

        [JsonProperty("X-APP-TOKEN")]
        public string[]? X_APP_TOKEN { get; set; }

        [JsonProperty("sec-ch-ua-platform")]
        public string[]? sec_ch_ua_platform { get; set; }

        [JsonProperty("Sec-Fetch-Site")]
        public string[]? Sec_Fetch_Site { get; set; }

        [JsonProperty("Sec-Fetch-Mode")]
        public string[]? Sec_Fetch_Mode { get; set; }

        [JsonProperty("Sec-Fetch-Dest")]
        public string[]? Sec_Fetch_Dest { get; set; }
    }
}
