﻿

namespace ndsApp1.Filters
{
    public class SessionFilter : ActionFilterAttribute
    {
        private readonly ILogger<SessionFilter> _logger;
        private readonly IDistributedCache _cache;

        public SessionFilter(ILogger<SessionFilter> logger/*, IDistributedCache cache*/)
        {
            _logger = logger;
            //_cache = cache;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //if (context == null || context.HttpContext == null || context.HttpContext.Request == null || context.HttpContext.Request.Headers == null)
            //{
            //    _logger.LogError("Headers are empty.");
            //    context.Result = new ObjectResult(context.ModelState)
            //    {
            //        Value = new JsonResult(new Error { error = "Headers are empty." }),
            //        StatusCode = 400
            //    };

            //    return;
            //}

            var headers = context.HttpContext.Request.Headers;
            var w = System.Text.Json.JsonSerializer.Serialize(headers);
            context.HttpContext.Items.Add("sessionString", w);


            //#region HeadersValidation
            //if (!headers.ContainsKey("X-APP-TOKEN"))
            //{
            //    _logger.LogError("No JWT token received");
            //    //context.Result = new ObjectResult(context.ModelState)
            //    //{
            //    //    Value = new JsonResult(new Error { error = "No header X-APP-Token in request." }),
            //    //    StatusCode = 400
            //    //};

            //    return;
            //}

            //if (!headers.ContainsKey("X-ACCOUNT-ID"))
            //{
            //    _logger.LogError("No account identifier provided in the request.");
            //    //context.Result = new ObjectResult(context.ModelState)
            //    //{
            //    //    Value = new JsonResult(new Error { error = "No header X-Account-Id in request." }),
            //    //    StatusCode = 400
            //    //};

            //    return;
            //}
            //#endregion

            //string token = headers["X-APP-TOKEN"];

            //#region SessionDataValidation
            //string sessionString;
            //SessionData session;

            //try
            //{
            //    sessionString = _cache.GetString(token);
            //    session = JsonConvert.DeserializeObject<SessionData>(sessionString);
            //}
            //catch (Exception)
            //{
            //    _logger.LogError("Failed to load session");
            //    //context.Result = new ObjectResult(context.ModelState)
            //    //{
            //    //    Value = new JsonResult(new Error { error = "Failed to load session" }),
            //    //    StatusCode = 403
            //    //};

            //    return;
            //}

            //if (session.token != headers["X-APP-TOKEN"].ToString())
            //{
            //    _logger.LogWarning("The token has lost the integrity");
            //    //context.Result = new ObjectResult(context.ModelState)
            //    //{
            //    //    Value = new JsonResult(new Error { error = "The user is not allowed on this endpoint" }),
            //    //    StatusCode = 403
            //    //};

            //    return;
            //}
            //else if (session.accountId != headers["X-ACCOUNT-ID"].ToString())
            //{
            //    _logger.LogWarning("The account id is not the same as the saved session");
            //    //context.Result = new ObjectResult(context.ModelState)
            //    //{
            //    //    Value = new JsonResult(new Error { error = "The user is not allowed on this endpoint " }),
            //    //    StatusCode = 403
            //    //};

            //    return;
            //}
            //else
            //{
            //    _logger.LogDebug("The user passed the Authorization");
            //    context.HttpContext.Items.Add("sessionString", sessionString);
            //}
            //#endregion
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {

        }
    }
}
